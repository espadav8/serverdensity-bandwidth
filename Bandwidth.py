import commands
from decimal import *

class Bandwidth (object):
    def __init__(self, agentConfig, checksLogger, rawConfig):
        self.agentConfig = agentConfig
        self.checksLogger = checksLogger
        self.rawConfig = rawConfig

    def run(self):
        bw = {}
        vnstat = commands.getoutput("vnstat --oneline -i eth0").split(';')
        bw['in'] = vnstat[8].split(' ')
        bw['out'] = vnstat[9].split(' ')
        bw['total'] = vnstat[10].split(' ')

        for i in bw:
            bw[i][0] = Decimal(bw[i][0])
            if bw[i][1] == 'KiB':
                bw[i][0] /= 1024
            if bw[i][1] == 'GiB':
                bw[i][0] *= 1024
            if bw[i][1] == 'TiB':
                bw[i][0] *= 1048576
            bw[i] = float(bw[i][0])

        return bw

if __name__ == "__main__":
    b = Bandwidth({}, {}, {})
    print b.run()
